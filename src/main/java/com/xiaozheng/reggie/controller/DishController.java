package com.xiaozheng.reggie.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozheng.reggie.common.R;
import com.xiaozheng.reggie.dto.DishDto;
import com.xiaozheng.reggie.entity.Category;
import com.xiaozheng.reggie.entity.Dish;
import com.xiaozheng.reggie.entity.DishFlavor;
import com.xiaozheng.reggie.service.CategoryService;
import com.xiaozheng.reggie.service.DishFlavorService;
import com.xiaozheng.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {

    @Autowired
    private DishService dishService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private RedisTemplate redisTemplate;

//    请求网址: http://localhost:8089/dish
//    请求方法: POST

    /**
     * 增加菜品
     * @param dishDto
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto){
        log.info(dishDto.toString());

        //调用dishService方法，将dishDto通过saveWithFlavors方法，方分别存入dish和disFlavor表中
        dishService.saveWithFlavors(dishDto);
        //更新时，清理redis数据
        String key = "dish_" + dishDto.getCategoryId() + "_1";
        redisTemplate.delete(key);

        return R.success("新增菜品成功");
    }


//    请求网址: http://localhost:8089/dish/page?page=1&pageSize=10
//    请求方法: GET
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize, String name){

        //page1指的是Dish表，page2指的是总表，也就是DishDto中的数据分页
        //因为有两张表，所以不能同时分页，只能将一个先分页，再把分好的拷贝到另一个总的dto上
        Page<Dish> page1 = new Page<>(page, pageSize);//这张分页不含有菜品分类

        Page<DishDto> page2 = new Page<>();//这张表是用来装入page1

        //条件构造器
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //当名字不等于空，执行查询，条件为%name%
        dishLambdaQueryWrapper.like(name != null, Dish::getName,name);

        //排序
        dishLambdaQueryWrapper.orderByDesc(Dish::getCreateTime);

        //执行分页查询
        dishService.page(page1,dishLambdaQueryWrapper);

        //对象拷贝，将page1拷贝到page2中，忽略其中的records，其中records指的是一条条查询的记录,具体如下
        //0 : Dish(id=1523149419648585729, name=炒年糕, categoryId=1397844303408574465, price=1200.00, code=, image=5a5fd69d-2ae9-416c-add9-5a4d9fdd8d1a.jpg, description=无, status=1, sort=0, createTime=2022-05-08T11:54:55, updateTime=2022-05-08T11:54:55, createUser=1, updateUser=1)
        //0 : Dish(id=1523148362709176322, name=青椒炒肉, categoryId=1397844303408574465, price=1200.00, code=, image=8124f7e3-aa0d-44c8-b1d0-f5c29a6c41f0.jpg, description=无, status=1, sort=0, createTime=2022-05-08T11:50:43, updateTime=2022-05-08T11:50:43, createUser=1, updateUser=1)
        BeanUtils.copyProperties(page1,page2,"records");

        //将一条条没有菜品分类的，dish表中的list集合获取出来
        List<Dish> records = page1.getRecords();

        List<DishDto> list = records.stream().map((item)->{
            //创建一个DishDto，用来存放一条条dish的records
            DishDto dishDto = new DishDto();
            //将records拷贝到dishDto
            BeanUtils.copyProperties(item,dishDto);

            Long categoryId = item.getCategoryId();//获取分类ID

            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);

            //如果菜品存在分类，则将菜品名字赋值道DishDto中的categoryName字段中
            if(category != null){
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }
            return dishDto;

        }).collect(Collectors.toList());

        //将dish中的一条条records存入dishDto中
        page2.setRecords(list);
        return R.success(page2);
    }





    @GetMapping("/{id}")
    public R<DishDto> get(@PathVariable Long id){

        log.info("获取到dish的ID为 {}",id);

        DishDto dishDto = dishService.getByIdWithFlavors(id);

        return R.success(dishDto);

    }

    /**
     * 修改菜品
     * @param dishDto
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto){
        log.info(dishDto.toString());

        //调用dishService方法，将dishDto通过saveWithFlavors方法，方分别存入dish和disFlavor表中
        dishService.updateWithFlavors(dishDto);

        //更新时，清理redis数据
        String key = "dish_" + dishDto.getCategoryId() + "_1";
        redisTemplate.delete(key);
        return R.success("修改菜品成功");
    }


    /**
     * 完善后的，开始后台只能查到不包含口味的，现在后台可查询包含口味，开始用的Dish，现在用的DishDto
     * @param dish
     * @return
     */
    @GetMapping("/list")
    public R<List<DishDto>> list(Dish dish){

        List<DishDto> dishDtoList = null;
        //先从redis中获取缓存
        String key = "dish_" +dish.getCategoryId() + "_" +dish.getStatus();
        dishDtoList = (List<DishDto>) redisTemplate.opsForValue().get(key);
        //如果存在则，则返回数据
        if (dishDtoList != null){
            return R.success(dishDtoList);
        }

        //=======================不存在，以下正常查询数据库===========================
        //构造查询条件
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(dish.getCategoryId() != null ,Dish::getCategoryId,dish.getCategoryId());
        //添加条件，查询状态为1（起售状态）的菜品
        queryWrapper.eq(Dish::getStatus,1);

        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);

        List<Dish> list = dishService.list(queryWrapper);

        dishDtoList= list.stream().map((item) -> {
            DishDto dishDto = new DishDto();

            BeanUtils.copyProperties(item,dishDto);

            Long categoryId = item.getCategoryId();//分类id
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);

            if(category != null){
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }

            //当前菜品的id
            Long dishId = item.getId();
            LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(DishFlavor::getDishId,dishId);
            //SQL:select * from dish_flavor where dish_id = ?
            List<DishFlavor> dishFlavorList = dishFlavorService.list(lambdaQueryWrapper);
            dishDto.setFlavors(dishFlavorList);
            return dishDto;
        }).collect(Collectors.toList());

        //如果redis没有缓存，则执行以上数据库查询，然后将数据放入redis
        redisTemplate.opsForValue().set(key,dishDtoList,60, TimeUnit.MINUTES);

        return R.success(dishDtoList);
    }

//    请求网址: http://localhost:8089/dish/status/0?ids=1523149419648585729
//    请求方法: POST

    /**
     * 批量停售，起售
     * @param status
     * @param ids
     * @return
     */
    @PostMapping("/status/{status}")
    public R<String> status(@PathVariable int status, Long[] ids){

        log.info("状态为{}，IDS为{}",status, ids);


        for (Long id : ids) {
            Dish dish = dishService.getById(id);
            dish.setStatus(status);
            dishService.updateById(dish);
        }


        return R.success("修改成功！");

    }


//    请求网址: http://localhost:8089/dish?ids=1523149419648585729
//    请求方法: DELETE

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> delete(Long[] ids){

        for (Long id : ids) {
            Dish dish = dishService.getById(id);

            dishService.removeById(dish);
        }

        return R.success("删除成功！");
    }

}
