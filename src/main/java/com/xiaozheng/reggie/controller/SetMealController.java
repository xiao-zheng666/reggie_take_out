package com.xiaozheng.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozheng.reggie.common.R;
import com.xiaozheng.reggie.dto.SetmealDto;
import com.xiaozheng.reggie.entity.Category;
import com.xiaozheng.reggie.entity.Dish;
import com.xiaozheng.reggie.entity.Setmeal;
import com.xiaozheng.reggie.entity.SetmealDish;
import com.xiaozheng.reggie.service.CategoryService;
import com.xiaozheng.reggie.service.SetMealDishService;
import com.xiaozheng.reggie.service.SetMealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 套餐管理
 */
@Slf4j
@RestController
@RequestMapping("/setmeal")
public class SetMealController {


    @Autowired
    private SetMealDishService setMealDishService;
    @Autowired
    private SetMealService setmealService;
    @Autowired
    private CategoryService categoryService;

//    请求网址: http://localhost:8089/setmeal
//    请求方法: POST

    /**
     * 新建套餐保存
     * @param setmealDto
     * @return
     */
    @CacheEvict(value = "setmealCache" ,allEntries = true)
    @PostMapping
    public R<String> save(@RequestBody SetmealDto setmealDto){

        log.info("套餐信息：{}" ,setmealDto);

        setmealService.saveWithDish(setmealDto);

        return R.success("添加套餐成功！");
    }


//    请求网址: http://localhost:8089/setmeal/1524981029004083201
//    请求方法: GET
    @GetMapping("/{setmelId}")
    public R<Setmeal> updateSetmeal(@PathVariable Long setmelId){

        Setmeal serviceById = setmealService.getById(setmelId);


        return R.success(serviceById);
    }



//    请求网址: http://localhost:8089/setmeal/page?page=1&pageSize=10
//    请求方法: GET

    /**
     * 套餐分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    protected R<Page> page(int page, int pageSize, String name){

        //分页构造器
        Page<Setmeal> pageInfo = new Page<>(page, pageSize);
        //包含（套餐分类）的page页面
        Page<SetmealDto> pageDto = new Page<>();



        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();

        setmealLambdaQueryWrapper.eq(name != null, Setmeal::getName,name);

        setmealLambdaQueryWrapper.orderByDesc(Setmeal::getUpdateTime);

        setmealService.page(pageInfo, setmealLambdaQueryWrapper);


        BeanUtils.copyProperties(pageInfo,pageDto,"records");

        List<Setmeal> records = pageInfo.getRecords();




        List<SetmealDto> list = records.stream().map((item) -> {
            SetmealDto setmealDto = new SetmealDto();
            //对象拷贝

            BeanUtils.copyProperties(item,setmealDto);
            //分类id
            Long categoryId = item.getCategoryId();
            //根据分类id查询分类对象
            Category category = categoryService.getById(categoryId);
            if(category != null){
                //分类名称
                String categoryName = category.getName();
                setmealDto.setCategoryName(categoryName);
            }
            return setmealDto;
        }).collect(Collectors.toList());

        pageDto.setRecords(list);
        return R.success(pageDto);
    }


    /**
     * 删除套餐
     * @param ids
     * @return
     */
    @CacheEvict(value = "setmealCache" ,allEntries = true)
    @DeleteMapping
    public R<String> delete(@RequestParam List<Long> ids){
        log.info("ids:{}",ids);

        setmealService.removeWithDish(ids);

        return R.success("套餐数据删除成功");
    }




//    请求网址: http://localhost:8089/setmeal/list?categoryId=1413342269393674242&status=1
//    请求方法: GET

    /**
     * 根据条件查询套餐数据
     * @param setmeal
     * @return
     */

    @GetMapping("/list")
    @Cacheable(value = "setmealCache",key = "#setmeal.categoryId + '_' +#setmeal.status")
    public R<List<Setmeal>> list(Setmeal setmeal){
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId() != null,Setmeal::getCategoryId,setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null,Setmeal::getStatus,setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        List<Setmeal> list = setmealService.list(queryWrapper);

        return R.success(list);
    }











//    请求网址: http://localhost:8089/setmeal/status/0?ids=1523335762760654849
//    请求方法: POST
    @PostMapping("/status/{status}")
    public R<String> status(@PathVariable int status, Long[] ids){

        log.info("状态为{}，IDS为{}",status, ids);


        for (Long id : ids) {
            Setmeal setmeal = setmealService.getById(id);
            setmeal.setStatus(status);
            setmealService.updateById(setmeal);
        }


        return R.success("修改成功！");

    }



}
