package com.xiaozheng.reggie.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozheng.reggie.common.BaseContext;
import com.xiaozheng.reggie.common.R;
import com.xiaozheng.reggie.dto.OrdersDto;
import com.xiaozheng.reggie.entity.OrderDetail;
import com.xiaozheng.reggie.entity.Orders;
import com.xiaozheng.reggie.service.OrderDetailService;
import com.xiaozheng.reggie.service.OrdersService;
import com.xiaozheng.reggie.service.UserService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrdersController {


//    请求网址: http://localhost:8089/order/submit
//    请求方法: POST


    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private UserService userService;

    /**
     * 用户下单
     *
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders) {
        log.info("订单数据：{}", orders);
        ordersService.submit(orders);
        return R.success("下单成功");
    }


//    请求网址: http://localhost:8089/order/userPage?page=1&pageSize=5
//    请求方法: GET


    @GetMapping("/userPage")
    public R<Page> userPage(int page, int pageSize) {



        Page<Orders> ordersPage = new Page<>(page, pageSize);
        Page<OrdersDto> ordersDtoPage = new Page<>();

        LambdaQueryWrapper<Orders> ordersLambdaQueryWrapper = new LambdaQueryWrapper<>();

        ordersLambdaQueryWrapper.eq(Orders::getUserId, BaseContext.getCurrentId());

        ordersLambdaQueryWrapper.orderByDesc(Orders::getOrderTime);

        //先通过用户名，找到订单信息
        List<Orders> list = ordersService.list(ordersLambdaQueryWrapper);

        List<OrdersDto> ordersDtoList = list.stream().map((item) -> {

            OrdersDto ordersDto = new OrdersDto();
            //将订单信息复制到OrderDto中
            BeanUtils.copyProperties(item, ordersDto);

            //获取订单ID
            Long orderId = item.getId();

            //通过订单ID查找，OrderDetail信息表单
            LambdaQueryWrapper<OrderDetail> lambdaQueryWrapper = new LambdaQueryWrapper<>();

            lambdaQueryWrapper.eq(orderId != null, OrderDetail::getOrderId, orderId);

            List<OrderDetail> orderDetailList = orderDetailService.list(lambdaQueryWrapper);

            //将订单详细信息，放入orderDetail中
            ordersDto.setOrderDetails(orderDetailList);

            return ordersDto;

        }).collect(Collectors.toList());


        ordersDtoPage.setRecords(ordersDtoList);
        return R.success(ordersDtoPage);

    }

//    请求网址: http://localhost:8089/order/page?page=1&pageSize=10
//    请求方法: GET


    @GetMapping("/page")
    public R<Page> Page(int page, int pageSize, String number,Date beginTime, Date endTime) {




        Page<Orders> ordersPage = new Page<>(page, pageSize);
        Page<OrdersDto> ordersDtoPage = new Page<>();


        LambdaQueryWrapper<Orders> ordersLambdaQueryWrapper = new LambdaQueryWrapper<>();


        //TODO 这里有点问题，写死了，应该缺个表，用来连接employee和user
        Long id = 1523528687310249985L;
        ordersLambdaQueryWrapper.eq(Orders::getUserId, id);

        ordersLambdaQueryWrapper.orderByDesc(Orders::getOrderTime);

        ordersLambdaQueryWrapper.eq(number != null,Orders::getNumber,number);
        ordersLambdaQueryWrapper.le(beginTime != null, Orders::getOrderTime,beginTime).ge(endTime != null, Orders::getOrderTime, endTime);

        //先通过用户名，找到订单信息
        List<Orders> list = ordersService.list(ordersLambdaQueryWrapper);

        List<OrdersDto> ordersDtoList = list.stream().map((item) -> {

            OrdersDto ordersDto = new OrdersDto();
            //将订单信息复制到OrderDto中
            BeanUtils.copyProperties(item, ordersDto);

            //获取订单ID
            Long orderId = item.getId();

            //通过订单ID查找，OrderDetail信息表单
            LambdaQueryWrapper<OrderDetail> lambdaQueryWrapper = new LambdaQueryWrapper<>();

            lambdaQueryWrapper.eq(orderId != null, OrderDetail::getOrderId, orderId);

            List<OrderDetail> orderDetailList = orderDetailService.list(lambdaQueryWrapper);

            //将订单详细信息，放入orderDetail中
            ordersDto.setOrderDetails(orderDetailList);

            return ordersDto;

        }).collect(Collectors.toList());


        ordersDtoPage.setRecords(ordersDtoList);
        return R.success(ordersDtoPage);

    }
}