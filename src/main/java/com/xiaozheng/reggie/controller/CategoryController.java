package com.xiaozheng.reggie.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaozheng.reggie.common.R;
import com.xiaozheng.reggie.entity.Category;
import com.xiaozheng.reggie.entity.Employee;
import com.xiaozheng.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/category")
public class CategoryController {


    @Autowired
    private CategoryService categoryService;

    /**
     * 菜品分类和套餐存入
     *
     * @param request
     * @param category
     * @return
     */
    @PostMapping
    public R<String> save(HttpServletRequest request, @RequestBody Category category) {

        categoryService.save(category);
        return R.success("菜品存入成功");
    }


    /**
     * 分类管理分页
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")        //page?page=1&pageSize=10
    public R<Page> page(int page, int pageSize) {
        //分页构造器
        Page<Category> pageInfo = new Page<>(page, pageSize);
        //条件构造器
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        //添加排序条件，根据sort进行排序
        queryWrapper.orderByAsc(Category::getSort);

        //分页查询
        categoryService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }

    /**
     * 根据ID删除，-----------------这里并没有用到restful风格，而是普通传参
     * restful：http://localhost:8089/category/1522494494899916801
     *
     * 普通：http://localhost:8089/category?ids=1522494494899916801
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> remove(Long ids){

        log.info("执行根据ID删除条件");
//        categoryService.removeById(ids);
        categoryService.remove(ids);
        return R.success("根据ID删除成功");

    }

    /**
     * 修改信息
     * @param category
     * @return
     */
    @PutMapping
    public R<String > update(@RequestBody Category category){

        categoryService.updateById(category);

        return R.success("修改分类信息成功");
    }




    //请求网址: http://localhost:8089/category/list?type=1
    //参数不能加 @RequestBody ,因为前段返回的是 type = 1 ；返回值为int，不是json。
    @GetMapping("/list")
    public R<List<Category>> list( Category category){

        //条件构造器
        LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //条件查询
        categoryLambdaQueryWrapper.eq( category.getType() != null, Category::getType,category.getType());

        //添加排序条件
        categoryLambdaQueryWrapper.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);

        List<Category> list = categoryService.list(categoryLambdaQueryWrapper);

        return R.success(list);
    }
}
