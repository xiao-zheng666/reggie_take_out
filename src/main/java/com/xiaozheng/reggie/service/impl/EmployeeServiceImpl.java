package com.xiaozheng.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaozheng.reggie.entity.Employee;
import com.xiaozheng.reggie.mapper.EmployeeMapper;
import com.xiaozheng.reggie.service.EmployeeService;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService{
}
