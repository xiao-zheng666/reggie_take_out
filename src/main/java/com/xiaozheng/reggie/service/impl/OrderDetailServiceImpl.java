package com.xiaozheng.reggie.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaozheng.reggie.entity.OrderDetail;
import com.xiaozheng.reggie.mapper.OrderDetailMapper;
import com.xiaozheng.reggie.service.OrderDetailService;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {
}
