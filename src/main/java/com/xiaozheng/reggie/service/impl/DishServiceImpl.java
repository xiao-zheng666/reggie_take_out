package com.xiaozheng.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaozheng.reggie.dto.DishDto;
import com.xiaozheng.reggie.entity.Dish;
import com.xiaozheng.reggie.entity.DishFlavor;
import com.xiaozheng.reggie.mapper.DishMapper;
import com.xiaozheng.reggie.service.DishFlavorService;
import com.xiaozheng.reggie.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {


    @Autowired
    private DishFlavorService dishFlavorService;

    /**
     * 新增菜品，同时保存对应的口味数据
     * @param dishDto
     */
    @Transactional
    @Override
    public void saveWithFlavors(DishDto dishDto) {


        //保存dish菜品，不包括 0: {name: "甜味", value: "["无糖","少糖","半糖","多糖","全糖"]", showOption: false}）
        this.save(dishDto);

        //此时，已将dishDto存入数据库中，其中flavors为null，其余已经赋值，并且新增菜品ID也通过雪花算法生成
        Long dishId = dishDto.getId();


        //获取口味
        List<DishFlavor> dishFlavors = dishDto.getFlavors();

        dishFlavors = dishFlavors.stream().map( (item) -> {
            //将dish菜品的ID，赋值给菜品口味
            item.setDishId(dishId);
            return item;
        }).collect(Collectors.toList());

        //保存菜品口味dishFlavors道数据库

        dishFlavorService.saveBatch(dishFlavors);
    }

    /**
     * 通过ID查询菜品信息，并在修改窗口显示
     * @param id
     */
    @Override
    public DishDto getByIdWithFlavors(Long id) {
        //查询菜品基本信息，从dish表，不包含口味
        Dish dish = this.getById(id);

        //把dish拷贝到disDao
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish,dishDto);

        //查询当前菜品的口味，从dish_Flavors表中查
        LambdaQueryWrapper<DishFlavor> dishFlavorLambdaQueryWrapper = new LambdaQueryWrapper<>();

        dishFlavorLambdaQueryWrapper.eq(DishFlavor::getDishId,dish.getId());

        List<DishFlavor> list = dishFlavorService.list(dishFlavorLambdaQueryWrapper);

        dishDto.setFlavors(list);

        return dishDto;
    }



    /**
     * 修改菜品，同时保存对应的口味数据
     * @param dishDto
     */
    @Transactional
    @Override
    public void updateWithFlavors(DishDto dishDto) {

        //更新dish信息
        this.updateById(dishDto);

        //清理当前菜品对应口味数据---dish_flavor表的delete操作
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(DishFlavor::getDishId,dishDto.getId());

        dishFlavorService.remove(queryWrapper);

        //添加当前提交过来的口味数据---dish_flavor表的insert操作
        List<DishFlavor> flavors = dishDto.getFlavors();

        flavors = flavors.stream().map((item) -> {
            item.setDishId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());

        dishFlavorService.saveBatch(flavors);


    }
}
