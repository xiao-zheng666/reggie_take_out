package com.xiaozheng.reggie.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaozheng.reggie.entity.AddressBook;
import com.xiaozheng.reggie.mapper.AddressBookMapper;
import com.xiaozheng.reggie.service.AddressBookService;
import org.springframework.stereotype.Service;

@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {
}
