package com.xiaozheng.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaozheng.reggie.common.CustomException;
import com.xiaozheng.reggie.dto.SetmealDto;
import com.xiaozheng.reggie.entity.Setmeal;
import com.xiaozheng.reggie.entity.SetmealDish;
import com.xiaozheng.reggie.mapper.SetmealMapper;
import com.xiaozheng.reggie.service.SetMealDishService;
import com.xiaozheng.reggie.service.SetMealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetMealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetMealService {


    @Autowired
    private SetMealDishService setMealDishService;
    /**
     * 新增套餐保存，分别存入SetMeal和SetMealDish表中
     * @param setmealDto
     */
    @Transactional
    @Override
    public void saveWithDish(SetmealDto setmealDto) {
        //保存套餐的基本信息，操作SetMeal，执行insert语句

        this.save(setmealDto);//当执行完这个语句时，MP已经通过雪花算法给setMealDto赋值了，因为其调用的是SetMeal服务，因此生成的是SetMeal的ID


        //保存套餐和菜品的相关信息，操作SetMeal_Dish表，执行insert语句

        //将setmealDto中的菜品信息封装到SetmealDish集合中
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();

        setmealDishes = setmealDishes.stream().map((item) -> {

            //上面执行了save方法后，setmealDto已经拥有了ID
            item.setSetmealId(setmealDto.getId());

            return item;
        }).collect(Collectors.toList());

        setMealDishService.saveBatch(setmealDishes);
    }


    /**
     * 删除套餐，同时需要删除套餐和菜品的关联数据
     * @param ids
     */
    @Transactional
    public void removeWithDish(List<Long> ids) {
        for (Long id : ids) {
            //select count(*) from setmeal where id in (1,2,3) and status = 1
            //查询套餐状态，确定是否可用删除
            LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper();
            queryWrapper.in(Setmeal::getId,ids);
            queryWrapper.eq(Setmeal::getStatus,1);

            int count = (int) this.count(queryWrapper);
            if(count > 0){
                //如果不能删除，抛出一个业务异常
                throw new CustomException("套餐正在售卖中，不能删除");
            }

            //如果可以删除，先删除套餐表中的数据---setmeal
            this.removeByIds(ids);

            //delete from setmeal_dish where setmeal_id in (1,2,3)
            LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.in(SetmealDish::getSetmealId,ids);
            //删除关系表中的数据----setmeal_dish
            setMealDishService.remove(lambdaQueryWrapper);
        }

    }
}
