package com.xiaozheng.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaozheng.reggie.entity.ShoppingCart;
import com.xiaozheng.reggie.mapper.ShoppingCartMapper;
import com.xiaozheng.reggie.service.ShoppingCartService;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {
}
