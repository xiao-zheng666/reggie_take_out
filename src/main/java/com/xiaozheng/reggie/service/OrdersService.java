package com.xiaozheng.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaozheng.reggie.entity.Orders;

public interface OrdersService extends IService<Orders> {

    void submit(Orders orders);
}
