package com.xiaozheng.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaozheng.reggie.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
