package com.xiaozheng.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaozheng.reggie.dto.SetmealDto;
import com.xiaozheng.reggie.entity.SetmealDish;

public interface SetMealDishService extends IService<SetmealDish> {


}
