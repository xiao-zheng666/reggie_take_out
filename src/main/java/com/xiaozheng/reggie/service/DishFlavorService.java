package com.xiaozheng.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaozheng.reggie.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
