package com.xiaozheng.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaozheng.reggie.dto.SetmealDto;
import com.xiaozheng.reggie.entity.Setmeal;

import java.util.List;

public interface SetMealService extends IService<Setmeal> {

    void saveWithDish(SetmealDto setmealDto);

    void removeWithDish(List<Long> ids);
}
