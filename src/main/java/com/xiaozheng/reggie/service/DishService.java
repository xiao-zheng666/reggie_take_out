package com.xiaozheng.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaozheng.reggie.dto.DishDto;
import com.xiaozheng.reggie.entity.Dish;

public interface DishService extends IService<Dish> {
     //新增菜品
     public void  saveWithFlavors(DishDto dishDto);
     //查询菜品
     public DishDto getByIdWithFlavors(Long id);
     //修改菜品
     public void updateWithFlavors(DishDto dishDto);
}
