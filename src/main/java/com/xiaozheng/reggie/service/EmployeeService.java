package com.xiaozheng.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaozheng.reggie.entity.Employee;

public interface EmployeeService extends IService<Employee> {
}
