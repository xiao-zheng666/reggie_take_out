package com.xiaozheng.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaozheng.reggie.entity.Category;

public interface CategoryService extends IService<Category> {
    void remove(Long ids);
}
