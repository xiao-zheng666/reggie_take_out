package com.xiaozheng.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaozheng.reggie.entity.User;

public interface UserService extends IService<User> {
}
