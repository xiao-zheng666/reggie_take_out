package com.xiaozheng.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.xiaozheng.reggie.common.BaseContext;
import com.xiaozheng.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebFilter(filterName = "loginCheckFilter", urlPatterns = "/*")
public class LoginCheckFilter implements Filter {

    //路径匹配器，支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest rq = (HttpServletRequest) request;
        HttpServletResponse rp = (HttpServletResponse) response;

        //1、获取本次请求的URI
        String requestURI = rq.getRequestURI();

        log.info("拦截到请求：{}",requestURI);

        //定义不需要处理的请求
        String[] urls = new String[]{
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/common/**",
                "/user/sendMsg",
                "/user/login"

        };
        //2、判断本次请求是否需要处理
        boolean check = check(urls, requestURI);

        //3、如果不需要处理，则直接放行
        if (check){
            log.info("本次请求{}不需要处理",requestURI);
            chain.doFilter(rq,rp);
            return;
        }
        //4、判断后台登录状态，如果已登录，则直接放行
        if (rq.getSession().getAttribute("employee") != null){
            log.info("用户已登录，用户id为：{}",rq.getSession().getAttribute("employee"));



            //将当前用户ID放入ThreadLocal中，会利用同一线程进行数据传递empId
            Long empId = (Long) rq.getSession().getAttribute("employee");
            BaseContext.setCurrentId(empId);

            chain.doFilter(rq,rp);
            return;
        }


        //4、判断用户登录状态，如果已登录，则直接放行
        if (rq.getSession().getAttribute("user") != null){
            log.info("用户已登录，用户id为：{}",rq.getSession().getAttribute("user"));



            //将当前用户ID放入ThreadLocal中，会利用同一线程进行数据传递empId
            Long userId = (Long) rq.getSession().getAttribute("user");
            BaseContext.setCurrentId(userId);

            chain.doFilter(rq,rp);
            return;
        }


        log.info("用户未登录");
        //5、如果未登录则返回未登录结果，通过输出流方式向客户端页面响应数据
        rp.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));

        return;
    }

    /**
     * 检查匹配登录路径是否
     * @param urls
     * @param requestURI
     * @return
     */

    public boolean check(String[] urls,String requestURI){
        for (String url : urls) {
            boolean match = PATH_MATCHER.match(url, requestURI);
            if(match){
                return true;
            }
        }
        return false;
    }
}
